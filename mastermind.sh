#!/bin/bash

letters='ABCDEFGH'

if [ "$1" == "--help" ]; then
    echo -e "\033[31m ###################################################################################### \033[0m"
    echo -e "\033[32m # ~       ~   ~~~     ~~~   ~~~~~  ~~~~~  ~~~~~    ~       ~  ~  ~      ~  ~ ~ ~     # \033[0m"
    echo -e "\033[33m # ~ ~   ~ ~  ~   ~   ~        ~    ~      ~    ~   ~ ~   ~ ~     ~~     ~  ~     ~   # \033[0m"
    echo -e "\033[34m # ~   ~   ~  ~   ~   ~        ~    ~      ~    ~   ~   ~   ~  ~  ~ ~    ~  ~      ~  # \033[0m"
    echo -e "\033[35m # ~       ~  ~   ~    ~~      ~    ~~~    ~~~~     ~       ~  ~  ~  ~   ~  ~      ~  # \033[0m"
    echo -e "\033[36m # ~       ~  ~ ~ ~      ~     ~    ~      ~   ~    ~       ~  ~  ~   ~  ~  ~      ~  # \033[0m"
    echo -e "\033[31m # ~       ~  ~   ~      ~     ~    ~      ~   ~    ~       ~  ~  ~    ~ ~  ~      ~  # \033[0m"
    echo -e "\033[32m # ~       ~  ~   ~      ~     ~    ~      ~    ~   ~       ~  ~  ~     ~~  ~      ~  # \033[0m"
    echo -e "\033[33m # ~       ~  ~   ~   ~~~      ~    ~~~~~  ~     ~  ~       ~  ~  ~      ~  ~ ~ ~     # \033[0m"
    echo -e "\033[34m ###################################################################################### \033[0m"
    echo "Règles :"
    echo "#1: Le but du jeu est de trouver la combinaison de code que l'adversaire ou l'ordinateur a choisit"
    echo "#2: Pour trouver le code, il vous faut écrire un code de 4 chiffres avec des lettres allant de A à H"
    echo "#3: C'est pas cool de tricher !"
else
    echo 'Bienvenue sur MasterMind, pour voir les règles : ctrl+C => ./mastermind.sh --help'
    echo 'Si vous connaissez les règles, veuillez taper 1 ou 2 sur votre clavier puis entrez: '
    echo '1- Jouer à deux avec un ami'
    echo '2- Jouer seul contre un ordinateur'
    while [[ -z $count ]] || [[ $count -eq "${count##[1-2]*}" ]]; do
        read -p 'Quel est votre choix ? ' count
        if [[ $count = 1 ]]; then
            shuffle=$(echo $shuffle)
            read -p 'Joueur 1: Veuillez rentrer un code secret avec les lettres ABCDEFGH : ' shuffle
            while [[ $shuffle != "${shuffle#*[^A-H]}" ]]; do
                read -p 'Joueur 1: Veuillez bien mettre quatre lettres valide : ' shuffle
            done
            code=${shuffle:0:4}
        elif [[ $count = 2 ]]; then
            shuffle2=$(echo $letters | fold -w1 | shuf | tr -d '\n')
            code=${shuffle2:0:4}
        fi
    done

    list=()

    for steps in {0..11}; do
        if [[ $count = 1 && $steps = 0 || $count = 2 && $steps = 0 ]]; then
            clear
        fi
        read -p 'Joueur 2: Entrez une supposition de code: ' response
        response=$(echo $response | sort -u)
        while [[ ${#response} != 4 || "$response" =~ [^A-Z] || $response != "${response#*[^A-H]}" ]]; do
            echo -e "\033[31m Joueur 2: Votre code doit contenir 4 caractères en MAJUSCULE \033[0m"
            read -p 'Joueur 2: Retentez votre chance: ' response
            response=$(echo $response)
        done
        if [ $response = $code ]; then
            echo -e "\033[32m Vous avez trouvé la combinaison! Félicitations! \033[0m"
            break
        elif [ $response != $code ] && [ $steps -eq 11 ]; then
            echo -e "\033[31m Vous avez utilisé vos 12 chances. \033[0m"
            echo 'La bonne réponse était: ' $code 
            break
        fi

        almost=0
        correct=0
        for i in {0..3}; do
            for j in {0..3}; do
                if [ ${code:$i:1} = ${response:$j:1} ]; then
                    (( almost++ ))
                fi
            done
        done

        for k in {0..3}; do
            if [ ${code:$k:1} = ${response:$k:1} ]; then
                (( correct++ ))
            fi
        done

        response+=$almost
        response+=$correct
        responselist+=($response)

        echo -----CODE--CONTIENT--CORRECT-----
        for index in ${!responselist[@]}; do
            value="${responselist[index]}"
            echo "$(($index + 1)):   ${value:0:4}      ${value:4:1}        ${value:5:1}"
        done
        echo -e "\033[33m Vous n'avez toujours pas trouvé \033[0m"
    done
fi